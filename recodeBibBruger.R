old_recodeBibBruger <- function (sgdata) {
  brugerCols <- grep("sidst benyttet", colnames(sgdata), value = TRUE)
  stopifnot(length(brugerCols) == 2)
  brugerColsIkkeBrugerCats <- c("Det er mere end 1 år siden", "Aldrig")
  tab <- table(sgdata[,brugerCols[1]], sgdata[,brugerCols[2]]
    #, useNA = "ifany"
    )
  ikkeBrugerTab <- tab[rownames(tab) %in% brugerColsIkkeBrugerCats,colnames(tab) %in% brugerColsIkkeBrugerCats]
  bibbruger <- ifelse(
    sgdata[,brugerCols[1]] %in% brugerColsIkkeBrugerCats & sgdata[,brugerCols[2]] %in% brugerColsIkkeBrugerCats,
    "Ikke-bruger",
    ifelse(
      is.na(sgdata[,brugerCols[1]]) | is.na(sgdata[,brugerCols[2]]),
      "Uvist",
      "Bruger"
      )
    )
  bibbrugertab <- table(bibbruger)
  stopifnot(bibbrugertab["Ikke-bruger"] == sum(ikkeBrugerTab))
  sgdata[,"bibbruger"] <- bibbruger
  return(sgdata)
}
recodeBibBruger <- function (sgdata) {
  brugerCols <- grep("sidst benyttet.*Fysisk", colnames(sgdata), value = TRUE)
  stopifnot(length(brugerCols) == 1)
  brugerColsIkkeBrugerCats <- c("Det er mere end 1 år siden", "Aldrig")
  tab <- table(sgdata[,brugerCols]
    #, useNA = "ifany"
    )
  ikkeBrugerTab <- tab[names(tab) %in% brugerColsIkkeBrugerCats]
  bibbruger <- ifelse(
    sgdata[,brugerCols] %in% brugerColsIkkeBrugerCats,
    "Ikke-bruger",
    ifelse(
      is.na(sgdata[,brugerCols]),
      "Uvist",
      "Bruger"
      )
    )
  bibbrugertab <- table(bibbruger)
  stopifnot(bibbrugertab["Ikke-bruger"] == sum(ikkeBrugerTab))
  sgdata[,"bibbruger"] <- bibbruger
  return(sgdata)
}

newvars <- "bibbruger"
mainFuncName <- "recodeBibBruger"
surveyNo <- 4895397
