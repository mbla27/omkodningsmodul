# Omkodningsmodul

Omkodningsmodul til analyseværktøjet på shiny.msvar.dk

For at tilføje en omkodning skal man:

1. Clone dette repo til en lokal mappe (hvis man kører lokalt, kør da bare i mappen 'omkodningsmodul' i selve værktøjet'. Kør evt. 'git submodule update --init', hvis mappen er tom)
2. Oprette filen med specifikationerne som nedenfor, fx recodeMyNewVar.R
3. Køre `git add . && git commit -am "Beskrivelse" && git push`
4. Tjek, om det virker i værktøjet (kan gå 1-2 minutter, inden det slår gennem)

Se [recodePct.R](recodePct.R) eller [recodeScenarier.R](recodeScenarier.R) for eksempler.

Alle filer skal indeholde funktionsdefinitionen og derefter til sidst:

```r
newvars <- paste0("scenarie",1:4) # Definition af nye variable
# (Der er her fire nye variable, nemlig scenarie1, ..., scenarie4)
# Hvis der ikke er nogle nye variable skal newvars være NULL
mainFuncName <- "recodeScenarier" # Information om hovedfunktionen, der skal kaldes
# Survey id'et (kan findes i SurveyGizmo)
surveyNo <- 4678298
```

Det er vigtigt, at selve omkodningsfunktionen ikke bliver kaldt. Omkodningsfunktionen skal returnere et helt datasæt.

Man kan downloade en .Rds-fil med rådata til sit omkodningsarbejde fra værktøjet. Det kan hentes med `sgdata <- readRDS(file = "~/Downloads/sgdata.Rds")`, hvis det fx ligger i en Downloads-mappe.

Hvis omkodningen ikke virker, bliver fejlbeskederne printet i værktøjet.
