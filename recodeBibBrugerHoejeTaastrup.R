recodeBibBruger2 <- function (sgdata) {
  brugerCols <- grep("Hvor ofte benytter du biblioteket", colnames(sgdata), value = TRUE)
  stopifnot(length(brugerCols) == 1)
  brugerColsIkkeBrugerCats <- c("Sjældnere end årligt", "Aldrig")
  tab <- table(sgdata[,brugerCols]
    #, useNA = "ifany"
    )
  ikkeBrugerTab <- tab[names(tab) %in% brugerColsIkkeBrugerCats]
  bibbruger <- ifelse(
    sgdata[,brugerCols] %in% brugerColsIkkeBrugerCats,
    "Ikke-bruger",
    ifelse(
      is.na(sgdata[,brugerCols]),
      "Uvist",
      "Bruger"
      )
    )
  bibbrugertab <- table(bibbruger)
  stopifnot(bibbrugertab["Ikke-bruger"] == sum(ikkeBrugerTab))
  sgdata[,"bibbruger"] <- bibbruger
  return(sgdata)
}

newvars <- "bibbruger"
mainFuncName <- "recodeBibBruger2"
surveyNo <- 5077271
